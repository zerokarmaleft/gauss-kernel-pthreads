#include "gaussian_kernel.h"

/*
 * Generate random numbers to fill the matrix A
 */
void
fill(float **A, int m, int n)
{
    srand(time(0));
    for (int i = 0; i < m; i++)
        for (int j = 0; j < n; j++)
            A[i][j] = (float) rand()/(float)RAND_MAX;
}

/*
 * Fill matrix A with ones for testing
 */
void
fill_ones(float **A, int m, int n)
{
    for (int i = 0; i < m; i++)
        for(int j = 0; j < n; j++)
            A[i][j] = 1.0;
}

/*
 * Display a small matrix A for testing
 */
void
print_matrix(float **A, int m, int n)
{
    for (int i = 0; i < m; i++)
        for(int j = 0; j < n; j++) {
            if (j == 0)
                printf("[ %4.2lf", A[i][j]);
            else if (j == n - 1)
                printf(" %4.2lf ]\n", A[i][j]);
            else
                printf(" %4.2lf", A[i][j]);
        }
    printf("\n");
}

/*
 * Compute the squared distance between two vectors as an independent
 * work unit of computing a Gaussian kernel concurrently
 */
static void *
squared_distance_fn(void *arg)
{
    int k = *((int *) arg);
    int r = N_COLS / num_threads;

    /* compute squared distance between two vectors */
    float sum;
    for (int i = 0; i < N_COLS; i++)
        for (int j = 0; j < N_COLS; j++) {
            sum = 0.0;

            for (int d = k * r; d < k * r + r; d++) {
                sum += pow(X[i][d] - X[j][d], 2);
            }

            thread[k].N[i][j] = sum;
        }

    return NULL;
}

int
main(int argc, char *argv[])
{
    /* parse command-line options */
    int opt;
    while ((opt = getopt(argc, argv, "t:")) != -1) {
        switch (opt) {
        case 't':
            num_threads = atoi(optarg);
            if ((N_COLS % num_threads) != 0) {
                fprintf(stderr, "Invalid argument: Work units cannot be divided evenly amongst %d threads\n",
                    num_threads);
                exit(EXIT_FAILURE);
            }
            break;
        default:
            abort();
        }
    }
    
    float **K;
    float sum;
    int s;
    void *result;

    /* allocate memory for training data */
    X = (float **) calloc(M_ROWS, sizeof(float *));
    if (X == NULL) {
        fprintf(stderr, "calloc() failed. Exiting...\n");
        exit(EXIT_FAILURE);
    }
    for (int i = 0; i < M_ROWS; i++) {
        X[i] = (float *) calloc(N_COLS, sizeof(float *));
        if (X[i] == NULL) {
            fprintf(stderr, "calloc() failed. Exiting...\n");
            exit(EXIT_FAILURE);
        }
    }

    /* allocate memory for Gaussian kernel */
    K = (float **) calloc(N_COLS, sizeof(float *));
    if (K == NULL) {
        fprintf(stderr, "calloc() failed. Exiting...\n");
        exit(EXIT_FAILURE);
    }
    for (int i = 0; i < N_COLS; i++) {
        K[i] = (float *) calloc(N_COLS, sizeof(float *));
        if (K[i] == NULL) {
            fprintf(stderr, "calloc() failed. Exiting...\n");
            exit(EXIT_FAILURE);
        }
    }
            
    /* initialize training data matrix */
    fill(X, M_ROWS, N_COLS);

    /* allocate memory for threads */
    thread = calloc(num_threads, sizeof(*thread));
    if (thread == NULL) {
        fprintf(stderr, "calloc() failed. Exiting...\n");
        exit(EXIT_FAILURE);
    }

    /* partition work units across threads */
    for (int i = 0; i < num_threads; i++) {
        thread[i].idx = i;

        /* allocate memory for per-thread intermediate data */
        thread[i].N = (float **) calloc(N_COLS, sizeof(float *));
        if (thread[i].N == NULL) {
            fprintf(stderr, "calloc() failed. Exiting...\n");
            exit(EXIT_FAILURE);
        }
        for (int j = 0; j < N_COLS; j++) {
            thread[i].N[j] = (float *) calloc(N_COLS, sizeof(float *));
            if (thread[i].N[j] == NULL) {
                fprintf(stderr, "calloc() failed. Exiting...\n");
                exit(EXIT_FAILURE);
            }
        }

        /* create thread */
        s = pthread_create(&thread[i].tid, NULL, squared_distance_fn, &thread[i].idx);
        if (s != 0) {
            fprintf(stderr, "pthread_create() failed. Exiting...\n");
            exit(EXIT_FAILURE);
        }
    }

    /* wait for threads to complete */
    for (int i = 0; i < num_threads; i++) {
        s = pthread_join(thread[i].tid, &result);
        if (s != 0) {
            fprintf(stderr, "pthread_join() failed. Exiting...\n");
            exit(EXIT_FAILURE);
        }
    }

    /* combine results of threads to final result */
    for (int i = 0; i < N_COLS; i++)
        for (int j = 0; j < N_COLS; j++) {
            sum = 0.0;

            for (int k = 0; k < num_threads; k++)                
                sum += thread[k].N[i][j] / (2 * pow(SIGMA, 2));

            K[i][j] = exp(-sum);
        }

    /* clean up allocated memory */
    for (int i = 0; i < M_ROWS; i++)
        free((void *) X[i]);
    free((void *) X);
    for (int i = 0; i < N_COLS; i++)
        free((void *) K[i]);
    free((void *) K);
    for (int i = 0; i < num_threads; i++) {
        for (int j = 0; j < N_COLS; j++)
            free((void *) thread[i].N[j]);
        free((void *) thread[i].N);
    }
    free((void *) thread);

    exit(EXIT_SUCCESS);
}
