#ifndef GAUSSIAN_KERNEL_H
#define GAUSSIAN_KERNEL_H

#define _GNU_SOURCE

#include <math.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

/*
 * Constants
 */
static const int M_ROWS        = 80000;
static const int N_COLS        = 1560;
static const double SIGMA      = 0.05;

/*
 * Global Variables shared between threads
 */
static int num_threads  = 8;
static float **X;

/*
 * Per-thread id and matrix
 */
static struct {
    pthread_t tid;
    int       idx;
    float     **N;
} *thread;

/*
 * Function Headers
 */
void fill(float **A, int m, int n);
void fill_ones(float **A, int m, int n);
void print_matrix(float **A, int m, int n);
static void *squared_distance_fn(void *arg);

#endif
