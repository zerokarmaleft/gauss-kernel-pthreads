Edward Cho
Assignment 2: Programming assignment on processes
CS 4323
Professor Michel Toulouse
February 20, 2012

=============== Introduction ===============

This program generates the Gaussian kernel for a specified dataset by
equally splitting the workload of computing the partial squared
distance between two vectors across a user-specified number of
threads.

The program utilizes a static 2d array for the input data, so it is
accessible by all threads in the thread worker function. A struct is
allocated to keep track of each Pthread id, the index (which
identifies the work unit for the thread), and the resulting matrix
from calculating squared distances. Once all the partial squared
distance matrices are computed, the main thread joins all of the
worker threads to generate a Gaussian kernel.

Mutexes are not used in this program since the algorithm guarantees
that each worker thread only reads from the training data and writes
results to its own exclusive data structure.

===============  Benchmarks  ===============

The program is executed with only a single thread by specifying the
command-line argument "-t" with 1.

$> ./gaussian_kernel -t 1
$>

Writing a concurrent program using threads instead of multiple
processes offers several advantages. Overall, the multi-threaded
version of the program runs faster than the multi-process version. The
Pthread API is also much simpler as it doesn't require a keeping track
of a shared memory segment key. Using the shared heap space of a
single process for multiple threads leads to cleaner, simpler code for
memory management.

The following benchmarks were taken on CSX0, which has 16 2.4 GHz
Intel Xeon processors, a very fast machine. I'm utilizing the same
machine I used for Assignment 2 (Processes).  The requirements for
Assignment 2 (Processes) had a smaller training matrix and the
benefits of the concurrent execution do not appear to be evident with
a data set this small.  When creating very large numbers of threads (n
>= 78), we begin to see a slight performance degradation.

|-------------------+----------+----------+----------|
| Number of threads | real     | user     | sys      |
|-------------------+----------+----------+----------|
|                 1 | 0m0.161s | 0m0.137s | 0m0.024s |
|                 2 | 0m0.145s | 0m0.139s | 0m0.020s |
|                 3 | 0m0.159s | 0m0.161s | 0m0.022s |
|                 4 | 0m0.143s | 0m0.143s | 0m0.023s |
|                 6 | 0m0.141s | 0m0.153s | 0m0.017s |
|                12 | 0m0.145s | 0m0.166s | 0m0.019s |
|                13 | 0m0.144s | 0m0.156s | 0m0.026s |
|                26 | 0m0.147s | 0m0.171s | 0m0.020s |
|                39 | 0m0.153s | 0m0.166s | 0m0.020s |
|                78 | 0m0.171s | 0m0.186s | 0m0.021s |
|               156 | 0m0.211s | 0m0.213s | 0m0.020s |
|-------------------+----------+----------+----------|

With threads, we are not limited to the size constraints of shared
memory segments. The following benchmarks change the number of rows
and columns for the 80000x1560 training matrix, which was originally
defined in Assignment 2 (Processes).  Here the performance improvement
from concurrent execution with threads are much more apparent, with
maximum performance achieved by creating 26 threads.

|-------------------+-----------+-----------+----------|
| Number of threads | real      | user      | sys      |
|-------------------+-----------+-----------+----------|
|                 1 | 0m32.279s | 0m31.960s | 0m0.315s |
|                 2 | 0m17.414s | 0m32.256s | 0m0.320s |
|                 3 | 0m12.481s | 0m32.508s | 0m0.334s |
|                 4 | 0m11.244s | 0m34.881s | 0m0.358s |
|                 6 | 0m9.738s  | 0m38.722s | 0m0.358s |
|                12 | 0m8.409s  | 0m55.473s | 0m0.853s |
|                13 | 0m8.252s  | 0m55.412s | 0m0.753s |
|                26 | 0m7.208s  | 1m0.779s  | 0m0.783s |
|                39 | 0m7.947s  | 1m1.628s  | 0m0.812s |
|                78 | 0m9.344s  | 1m6.694s  | 0m1.035s |
|               156 | 0m13.796s | 1m16.874s | 0m1.623s |
|-------------------+-----------+-----------+----------|

Using gprof, we can examine where our time is spent in each function
vs the number of threads created.  This is done by compiling with the
-pg switch:

$> gcc -Wall -std=c99 gaussian_kernel.c -lm -lpthread -pg -o gaussian_kernel
$> ./gaussian_kernel -t 1
$> gprof gaussian_kernel gmon.out

|-------------------+--------+---------------------|
| Number of threads | % time | name                |
|-------------------+--------+---------------------|
|                 1 |  96.96 | squared_distance_fn |
|                   |   3.12 | fill                |
|                   |   0.16 | main                |
|                 2 |  97.15 | squared_distance_fn |
|                   |   2.87 | fill                |
|                   |   0.23 | main                |
|                 3 |  97.06 | squared_distance_fn |
|                   |   2.83 | fill                |
|                   |   0.36 | main                |
|                 4 |  97.92 | squared_distance_fn |
|                   |   1.98 | fill                |
|                   |   0.35 | main                |
|                 6 |  97.53 | squared_distance_fn |
|                   |   2.40 | fill                |
|                   |   0.32 | main                |
|                12 |  98.12 | squared_distance_fn |
|                   |   1.45 | fill                |
|                   |   0.68 | main                |
|                13 |  97.69 | squared_distance_fn |
|                   |   1.81 | fill                |
|                   |   0.76 | main                |
|                26 |  96.70 | squared_distance_fn |
|                   |   2.01 | main                |
|                   |   1.54 | fill                |
|                39 |  94.31 | squared_distance_fn |
|                   |   3.49 | main                |
|                   |   2.45 | fill                |
|                78 |  93.10 | squared_distance_fn |
|                   |   5.19 | main                |
|                   |   1.95 | fill                |
|               156 |  90.64 | squared_distance_fn |
|                   |   8.27 | main                |
|                   |   1.35 | fill                |
|-------------------+--------+---------------------|

The loop to combine results in main() begins to create more overhead.
Once it overtakes fill(), the overhead from combining more and more
results becomes non-trivial and degrades the performance of the entire
program.

===============     Bugs     ===============

I ran into two difficulties arose while developing this program. When
creating loops, by passing the the address of the loop counter to the
thread function, I created a race condition. The behavior was erratic
and inconsistent on every run. Explicitly copying the value to each
thread's exlusive data structure easily fixed this problem.

I checked all memory allocated was freed using Valgrind.
