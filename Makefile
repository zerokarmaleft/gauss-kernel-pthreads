all:
	gcc -Wall -std=c99 gaussian_kernel.c -lm -lpthread -o gaussian_kernel

clean:
	rm -rf gaussian_kernel
